import * as React from "react"

interface Props {
}

interface State {
}

export class App extends React.Component<Props, State> {

    public render() {

        const PLAYER1 = 'fa-circle';
        const PLAYER2 = 'fa-times';
        let round = 1;
        let winner = '';

        var board = [
            ['', '', ''],
            ['', '', ''],
            ['', '', '']
        ];

        const combinations = [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8],
            [0, 4, 8],
            [2, 4, 6]
        ]

        function start(f) {
            const boxes = [... document.querySelectorAll('.box')];
            boxes.forEach(box => box.addEventListener('click', pick));
            console.log(document.querySelectorAll('.box'));


            function pick(event) {
                const {row, column} = event.target.dataset;
                const turn = round % 2 === 0 ? PLAYER2 : PLAYER1;
                if (board[row][column] !== '') return;
                if (winner !== '') return;
                event.target.classList.add(turn);
                board[row][column] = turn;
                roundCounter()
                round++;


                console.log(check());
            }

            function check() {
                const result = board.reduce((total, row) => total.concat(row));
                let moves = {
                    'fa-times': [],
                    'fa-circle': []
                };
                result.forEach((field, index) => moves[field] ? moves[field].push(index) : null);
                combinations.forEach(combination => {
                    if (combination.every(index => moves[PLAYER1].indexOf(index) > -1)) {
                        winner = 'Winner : Player 1';
                    }
                    if (combination.every(index => moves[PLAYER2].indexOf(index) > -1)) {
                        winner = 'Winner : Player 2';
                    }
                });

                console.log(result);

                return winner
            }


            if (f == 'restart') {

                round = 1;
                winner = '';
                board = [
                    ['', '', ''],
                    ['', '', ''],
                    ['', '', '']
                ];

                boxes.forEach(box => box.className = "box fa");
                roundCounter()

            }

            function roundCounter() {
                document.getElementById("number").innerHTML = round.toString();
            }
        }


        return (
            <div className="App">
                <nav className="navigation">
                    <button className="start button" onClick={start}>start</button>
                    <div className='turns-container'>
                        <div className="turn-number">Turn number: <div id="number">0</div></div>
                    </div>
                    <button className="restart button" onClick={() => start('restart')}>restart</button>
                </nav>
                <div className="board">
                    <div className="box fa" data-row="0" data-column="0"></div>
                    <div className="box fa" data-row="0" data-column="1"></div>
                    <div className="box fa" data-row="0" data-column="2"></div>
                    <div className="box fa" data-row="1" data-column="0"></div>
                    <div className="box fa" data-row="1" data-column="1"></div>
                    <div className="box fa" data-row="1" data-column="2"></div>
                    <div className="box fa" data-row="2" data-column="0"></div>
                    <div className="box fa" data-row="2" data-column="1"></div>
                    <div className="box fa" data-row="2" data-column="2"></div>
                </div>

            </div>
        );
    }
}
